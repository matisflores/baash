#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>


int
parsePath (char* folders[]);
int
fromKeyboard (char* argv[], char* input);
void
searchFile (char* file, char* folders[], char* commandFile);
int
isBackground (char* argv[]);
int
isRedirect (char* argv[], char filename[]);
int
checkPipe (char* argv[], char* argv1[], char* argv2[]);
void
toFile (char filename[]);
void
fromFile (char filename[]);
void
execPipeline (char* argv1[], char* argv2[], char* folders[]);

int
main ()
{
  int pid, waiting, toBackground;
  int argC;
  char* argV[20];
  char* argv1[20];
  char* argv2[20];
  char commandFile[256];
  char command[256];
  char hostname[20];
  char* user;
  char *folders[20];

  gethostname (hostname, 20);
  user = getlogin ();
  parsePath(folders);

  while (1)
    {
      int isPiped;
      strcpy (command, "\n");
      waiting = 0;
      isPiped = 0;

      printf ("%s@%s:%s$ ", user, hostname, getcwd (NULL, 50));
      fgets (command, 256, stdin);

      if (!strcmp (command, "\n"))
	{
	  printf ("\n");
	  continue;
	}
      else
	{
	  int isRedirected = 0, isPipeline = 0;
	  char filename[50];
	  argC = fromKeyboard (argV, command);

	  if (!strcmp (command, "exit"))
	    return (0);
	  if (!strcmp (argV[0], "cd"))
	    {
	      chdir (argV[1]);
	      continue;
	    }

	  isPipeline = checkPipe (argV, argv1, argv2);
	  isRedirected = isRedirect (argV, filename);

	  toBackground = isBackground (argV);
	  if (toBackground)
	    {
	      argV[argC - 1] = NULL;
	      argC--;
	    }

	  searchFile (argV[0], folders, commandFile);
	  if (commandFile[0] == 'X')
	    printf ("Command not found\n");
	  else
	    {
	      pid = fork ();
	      if (pid < 0)
		{
		  perror ("Error - Child creation");
		  exit (1);
		}
	      else if (pid == 0)
		{
		  if (isRedirected == 2)
		    {
		      toFile (filename);
		    }
		  else if (isRedirected == 1)
		    {
		      freopen (filename, "r", stdin);
		    }
		  else if (isPipeline == 1)
		    {
		      execPipeline (argv1, argv2, folders);
		      isPiped = 1;
		    }
		  if (!isPiped)
		    {
		      execv (commandFile, argV);
		      perror (commandFile);
		      exit (1);
		    }
		}
	      else
		{
		  waiting = -1;
		}
	      if (toBackground)
		waitpid (pid, &waiting, WNOHANG);
	      else
		waitpid (pid, &waiting, 0);
	    }
	}
    }
  return (0);
}

int
parsePath (char* folders[])
{
  int counter;
  char* var = getenv ("PATH");

  folders[0] = strtok (var, ":");
  for (counter = 1; counter < 20; counter++)
    {
      folders[counter] = strtok (NULL, ":");
      if (folders[counter] == NULL)
	break;
    }
  strtok (NULL, ":");
  return (counter + 1);
}

int
fromKeyboard (char* argv[], char* input)
{
  int counter = 0;

  argv[0] = strtok (input, " \n");
  for (counter = 1; counter < 20; counter++)
    {
      argv[counter] = strtok (NULL, " \n");
      if (argv[counter] == NULL)
	break;
    }
  return (counter);
}

void
searchFile (char* file, char* folders[], char* commandFile)
{
  char commandPath[50];
  int result;
  char currentDir[50] = "";
  char* archivo;
  strcpy (commandPath, file);

  if (file[0] == '/' || (file[0] == '.' && file[1] == '.' && file[2] == '/'))
    {
      char* dir;
      char* nextDir;
      int ready = 0;

      if (file[0] == '/')
	currentDir[0] = '/';
      dir = strtok (file, "/");
      nextDir = strtok (NULL, "/");

      if (nextDir != NULL)
	strcat (currentDir, dir);
      else
	{
	  nextDir = dir;
	  ready = 1;
	}

      while ((nextDir != NULL) && !ready)
	{
	  dir = nextDir;
	  nextDir = strtok (NULL, "/");
	  strcat (currentDir, "/");
	  if (nextDir != NULL)
	    strcat (currentDir, dir);
	}
      archivo = dir;
    }
  else if (file[0] == '.' && file[1] == '/')
    {
      getcwd (currentDir, 50);
      strcat (currentDir, "/");
      archivo = strtok (file, "/");
      archivo = strtok (NULL, "/");
    }
  else
    {
      int i;
      char aux[50];
      for (i = 0; i < 20; i++)
	{
	  if (folders[i] == NULL)
	    break;
	  strcpy (aux, folders[i]);
	  strcat (aux, "/");
	  strcat (aux, file);
	  result = access (aux, F_OK);
	  if (!result)
	    {
	      strcpy (commandFile, aux);
	      return;
	    }
	}
      commandFile[0] = 'X';
      return;
    }

  strcat (currentDir, archivo);
  result = access (currentDir, F_OK);
  if (!result)
    strcpy (commandFile, currentDir);
  else
    commandFile[0] = 'X';
}

int
isBackground (char* argv[])
{
  int i;
  for (i = 0; i < 20; i++)
    {
      if (argv[i] == NULL)
	break;
    }
  if (!strcmp (argv[i - 1], "&"))
    return (1);
  return (0);
}

int
isRedirect (char* argv[], char filename[])
{
  int i;
  for (i = 0; i < 20; i++)
    {

      if (argv[i] == NULL)
	{
	  filename = NULL;
	  return (0);
	}
      else if (!strcmp (argv[i], "<"))
	{
	  strcpy (filename, argv[i + 1]);
	  argv[i] = NULL;
	  argv[i + 1] = NULL;
	  return (1);
	}
      else if (!strcmp (argv[i], ">"))
	{
	  strcpy (filename, argv[i + 1]);
	  argv[i] = NULL;
	  argv[i + 1] = NULL;
	  return (2);
	}
    }
  return (0);
}

void
toFile (char filename[])
{
  int file;

  file = open (filename, O_WRONLY | O_CREAT | O_TRUNC, S_IWUSR | S_IRUSR);
  if (file < 0)
    {
      perror ("open");
      exit (1);
    }
  close (STDOUT_FILENO);
  if (dup (file) < 0)
    {
      perror ("dup");
      exit (1);
    }
  close (file);
}

void
fromFile (char filename[])
{
  int file;

  close (STDIN_FILENO);
  file = open (filename, O_RDONLY, S_IWUSR | S_IRUSR);
  if (file < 0)
    {
      perror ("open");
      exit (1);
    }
  if (dup (file) < 0)
    {
      perror ("dup");
      exit (1);
    }
  close (file);
}

int
checkPipe (char* argv[], char* argv1[], char* argv2[])
{
  int indexArg, indexArg2;

  for (indexArg = 0; argv[indexArg] != NULL; indexArg++)
    {
      int aux = strcmp ("|", argv[indexArg]);
      if (aux == 0)
	break;
      argv1[indexArg] = (char*) malloc (strlen (argv[indexArg] + 1));
      strcpy (argv1[indexArg], argv[indexArg]);
    }
  argv1[indexArg] = '\0';

  if (argv[indexArg] == NULL)
    return (0);

  indexArg++;
  for (indexArg2 = 0; argv[indexArg] != NULL; indexArg2++)
    {
      if (argv[indexArg] == NULL)
	break;
      argv2[indexArg2] = (char*) malloc (strlen (argv[indexArg] + 1));
      strcpy (argv2[indexArg2], argv[indexArg]);
      indexArg++;
    }
  argv2[indexArg2] = '\0';
  return (1);
}

void
execPipeline (char* argv1[], char* argv2[], char* folders[])
{
  char command[256];

  int fd[2];
  pipe (fd);
  if (fork () == 0)
    {
      close (fd[0]);
      dup2 (fd[1], 1);
      close (fd[1]);
      searchFile (argv1[0], folders, command);
      execv (command, argv1);
      perror (command);
      exit (1);
    }
  else
    {
      close (fd[1]);
      dup2 (fd[0], 0);
      close (fd[0]);
      searchFile (argv2[0], folders, command);
      execv (command, argv2);
      perror (command);
      exit (1);
    }
}
