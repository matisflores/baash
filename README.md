# Ksamp

**Laboratorio 2: Un Baash**

*Objetivos*

1. Utilizar los mecanismos de concurrencia y comunicación UNIX.
2. Implementar de manera sencilla un intérprete de línea de comandos (shell) al estilo de Bourne shell.

*Introducción*

La interfaz más tradicional de un sistema operativo tipo UNIX es el `intérprete de
línea de comandos`. Este programa, que se ejecuta en modo usuario, funciona en
cualquier UNIX que soporte interface de caracteres y su función es aceptar
comandos ingresados por entrada estandar (teclado), parsearlos, ejecutar la
órden y mostrar el resultado en la salida estandar (pantalla), para luego volver a
repetir el proceso.
Por defecto UNIX ejecuta un proceso shell cada vez que un usuario interactivo
ingresa al sistema. Aunque esto puede ser configurado de otra manera (ver el
último campo de cada línea del archivo `/etc/passwd`), en la mayoría de los casos
luego de ingresar nuestro nombre de usuario y contraseña, el proceso que
maneja el ingreso de usuarios genera un proceso hijo que ejecuta un shell, con el
uid/gid (identificador de usuario y grupo) correspondiente al usuario. En este
momento la pantalla se suele presentar de la siguiente manera:

**[juan@hal juan]$**

Después de este texto inicial llamado prompt, que contiene información de
entorno como por ejemplo el nombre del usuario, el nombre del host y el último
tramo del directorio corriente, el shell espera datos a través de la stdin que
normalmente se asocia al dispositivo teclado. Podemos escribir el comando que
deseamos que el shell ejecute, e iniciar la ejecución ingresando el caracter
NEWLINE '\n' generalmente asociado con la tecla Enter o Return.

**[juan@hal juan]$ sleep 10**

Hará que el shell ejecute un proceso con el programa binario que se encuentra
en `/bin/sleep`, pasándole el argumento "10".